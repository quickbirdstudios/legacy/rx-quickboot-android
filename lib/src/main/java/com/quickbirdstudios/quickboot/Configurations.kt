package com.quickbirdstudios.quickboot

/**
 * Created by sebastiansellmair on 14.02.18.
 */
fun QuickConfiguration.enableVerboseDisposeBag(enable: Boolean): QuickConfiguration {
    DisposeBag.verbose = enable
    return this
}